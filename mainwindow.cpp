#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QString>
#include<fstream>
#include<sstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	BacklightFileLocation = "/sys/class/backlight/nv_backlight/brightness";
	std::ifstream BacklightFile(BacklightFileLocation);
	std::stringstream ss;
	ss << BacklightFile.rdbuf();
	ui->horizontalSlider_Brightness->setSliderPosition(QString(ss.str().c_str()).toInt());
	BacklightFile.close();
}

MainWindow::~MainWindow(){
	delete ui;
}

void MainWindow::on_pushButton_Set_clicked(){
	std::ofstream BacklightFile(BacklightFileLocation);
	BacklightFile << QString::number(ui->horizontalSlider_Brightness->sliderPosition()).toStdString();
	BacklightFile.close();
}

void MainWindow::on_horizontalSlider_Brightness_valueChanged(int value){
	ui->label_Brightness->setGeometry(5 + (int)(2.8*value),
	                                  ui->label_Brightness->geometry().y(),
	                                  ui->label_Brightness->geometry().width(),
	                                  ui->label_Brightness->geometry().height());
	ui->label_Brightness->setText(QString::number(value));
}
