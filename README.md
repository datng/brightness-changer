# Brightness Changer
## Purpose
This program is written to change screen brightness on old laptops running on Nvidia GPUs, for which there is no longer support from Nvidia (the driver is no longer updated, nouveau installed instead).

## Dependencies
- Linux
- Qt

## Compilation
- On OpenSUSE: `qmake-qt5 && make`
- On other Linux systems the process should be similar

## Usage
- Run the compiled program as root user, or give yourself write permision in `/sys/class/backlight/nv_backlight/brightness` and run normally
- Drag the slider to your desired brightness level
- Click "Set"