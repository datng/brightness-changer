#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_pushButton_Set_clicked();
	void on_horizontalSlider_Brightness_valueChanged(int value);

private:
	Ui::MainWindow *ui;
	std::string BacklightFileLocation;
};

#endif // MAINWINDOW_H
